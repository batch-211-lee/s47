console.log(document);
console.log(document.querySelector("#txt-first-name"));
//result - element from html
/*
	Document - refers to the whole web page
	querySelector - used to select a specific element (object) as long as it is inside the html tag(HTML Element);
	-takes a string input that is formatted like CSS selector
	-can select elements regardless if the string is an id, a class, or a tag as long as the element is existing in the webpage
*/

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

/*
	event - actions the user is doing in our webpage.
	addEventListener - function that lets the webpage to listen to the events performed by the user
		-takes two arguments
			-string- the event to which the HTML element will listen, these are predetermined
			-function - executed by the element once the event (first argument) is triggered
*/

// txtFirstName.addEventListener("keyup", (event)=>{
// 	spanFullName.innerHTML = txtFirstName.value
// });

// txtFirstName.addEventListener("keyup", (event)=>{
// 	console.log(event);
// 	console.log(event.target);
// 	console.log(event.target.value)
// });

// txtlastName.addEventListener("keyup", (event)=>{
// 	spanFullName.innerHTML = txtLastName.value
// });

const updateFullName = ()=>{
	let firstName = txtFirstName.value;
	let lastName = txtLastName.value;
	spanFullName.innerHTML = `${firstName} ${lastName}`
}

txtFirstName.addEventListener("keyup", (event)=>{
	updateFullName()
});

txtLastName.addEventListener("keyup", (event)=>{
	updateFullName()
});